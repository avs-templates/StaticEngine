/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package site.pages

import template.html.HtmlTemplate

object About: HtmlTemplate("about.html", "pages", "About", "About site") {
    override fun getArticle() = """
            <article>
                <ul>
                    <li>We use <a href='http://www.fatcow.com/free-icons'>Farm-Fresh Web Icons</a></li>
                </ul>
            </article>
        """
}