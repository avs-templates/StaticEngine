/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package site

import site.articles.Article
import site.pages.About
import template.PageTemplate
import template.html.HtmlTemplate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

object Rss: PageTemplate("rss.xml", "") {

    private val dateFormatter = DateTimeFormatter
            .ofPattern("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH)
            .withZone(ZoneId.systemDefault())

    private val rss = """
            <rss version="2.0">
                <channel>
                <title>${Strings.title}</title>
                <link>${Site.url}</link>
                <description>${Strings.description}</description>

                ${Index.getRssItem()}
                ${About.getRssItem()}
                ${Article.getRssItem()}

                </channel>
             </rss>
        """


    private val hash by lazy { getHash(rss) }

    override fun getCode(): String{
        val hash = getHash(rss)
        return """
                <?xml version="1.0" encoding="UTF-8"?>
                <!-- $hash -->
                $rss
            """
    }

    override fun isHashEqual(source: List<String>) =
            source.count() > 2 && source[1].trimStart(' ', '\t').trimEnd(' ', '\t') == "<!-- $hash -->"

    private fun HtmlTemplate.getRssItem() = getRssItem(this.title, this.getUrl(), this.description, this.date)
    private fun getRssItem(title: String, link: String, description: String, pubDate: LocalDateTime? = null) =
            """
                <item>
                    <title>$title</title>
                    <link>$link</link>
                    <description>$description</description>
                    ${if(pubDate != null) "<pubDate>${pubDate.format(dateFormatter)}</pubDate>" else ""}
                </item>
            """
}