/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package site

import template.PageTemplate

object Robots : PageTemplate("robots.txt", "") {

    private val code = """
            User-agent: *
            Disallow:
            Sitemap: ${Site.url}/sitemap.xml
        """

    private val hash by lazy { getHash(code) }

    override fun getCode() =
        """
            # $hash
            $code
        """

    override fun isHashEqual(source: List<String>) =
            source.count() > 0 && source[0].trimStart(' ', '\t').trimEnd(' ', '\t') == "# $hash"

}