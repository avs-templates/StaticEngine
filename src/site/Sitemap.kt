/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package site

import site.articles.Article
import site.pages.About
import template.PageTemplate
import template.html.HtmlTemplate
import java.time.LocalDate
import java.time.format.DateTimeFormatter

object Sitemap: PageTemplate("sitemap.xml", "") {
    private val code = """
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                ${Index.sitemapXml(priority = Priority.P10)}
                ${About.sitemapXml()}
                ${Article.sitemapXml()}
            </urlset>
        """
    private val hash by lazy { getHash(code) }

    override fun getCode() =
        """
            <?xml version="1.0" encoding="UTF-8"?>
            <!-- $hash -->
            $code
        """

    private fun HtmlTemplate.sitemapXml(changefreq: ChangeFreq? = null, priority: Priority? = null)
            = sitemapUrlXml(this.getUrl(), this.date?.toLocalDate(), changefreq, priority)

    fun sitemapUrlXml(url: String, lastmod: LocalDate? = null, changefreq: ChangeFreq? = null, priority: Priority? = null) =
        """
            <url>
                <loc>$url</loc>
                ${if(lastmod != null) "<lastmod>${lastmod.format(DateTimeFormatter.ISO_LOCAL_DATE)}</lastmod>" else ""}
                ${if(changefreq != null) "<changefreq>${changefreq.toString().toLowerCase()}</changefreq>" else ""}
                ${if(priority != null) "<priority>${priority.value}</priority>" else ""}
            </url>
        """

    enum class ChangeFreq { Always, Hourly, Daily, Weekly, Monthly, Yearly, Never }
    enum class Priority(val value: String) {
        P10("1.0"),
        P9("0.9"),
        P8("0.8"),
        P7("0.7"),
        P6("0.6"),
        P5("0.5"),
        P4("0.4"),
        P3("0.3"),
        P2("0.2"),
        P1("0.1"),
        P0("0.0")
    }

    override fun isHashEqual(source: List<String>) =
            source.count() > 2 && source[1].trimStart(' ', '\t').trimEnd(' ', '\t') == "<!-- $hash -->"
}