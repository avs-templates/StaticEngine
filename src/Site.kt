/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import site.*
import site.articles.*
import site.pages.*
import java.io.File
import java.nio.charset.StandardCharsets
import java.io.IOException
import java.nio.file.StandardCopyOption
import java.nio.file.Files
import java.nio.file.Paths

object Site {

    private const val output = "public"

    const val url = "http://www.example.com"

    private val pageForGenerate = arrayOf(
            Index,
            About,
            Article,
            Robots, Rss, Sitemap, Style
    )

    private const val appInfoText = """
        StaticEngine - simple static site generator
        Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <http://www.gnu.org/licenses/>.
    """

    @JvmStatic fun main(args: Array<String>) {
        println(appInfoText)

        val path = System.getProperty("user.dir")

        val outputPath = "$path/$output"

        // check output directory
        File(outputPath).apply {
            if(!exists()){
                mkdirs()
            }
        }

        // copy static files
        copyDir(javaClass.getResource("static").file, outputPath)

        pageForGenerate.forEach {
            val filePath = outputPath + it.getFullPath()
            Log("Proceed file: $filePath")
            val file = File(filePath)
            if (file.exists() && file.isFile && it.isHashEqual(file.readLines())) {
                Log(" -> Equal hash")
            } else {
                val parentDir = file.parentFile
                if (!parentDir.exists()) {
                    Log(" -> Directory creating")
                    parentDir.mkdirs()
                }
                Log(" -> File rewriting")
                file.writeText(it.toString(), StandardCharsets.UTF_8)
            }
        }
    }

    private fun copyDir(from: String, to: String, overwrite: Boolean = false) {
        try {
            Log("Copy static files (overwrite $overwrite)")
            Files.walk(Paths.get(from)).forEach { fromPath ->
                val toPath = Paths.get(to, fromPath.toString().substring(from.length))
                try {
                    if (fromPath.toString() != from){
                        val fromFile = fromPath.toFile()
                        val toFile = toPath.toFile()
                        if(!overwrite && toFile.exists() && toFile.totalSpace == fromFile.totalSpace){
                            Log(" -> Skip file $toPath")
                        }else{
                            Log(" -> Copy file $toPath")
                            if(toPath.toFile().exists()){
                                Files.copy(fromPath, toPath, StandardCopyOption.REPLACE_EXISTING)
                            }else{
                                Files.copy(fromPath, toPath)
                            }
                        }
                    }
                } catch (e: IOException) {
                    Log(" -> Copy file error:",Log.Level.Error)
                    e.printStackTrace()
                }
            }
        } catch (e: IOException) {
            Log(" -> Walk files error:",Log.Level.Error)
            e.printStackTrace()
        }

    }
}