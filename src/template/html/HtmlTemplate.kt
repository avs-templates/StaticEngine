/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package template.html

import site.Style
import template.BaseTemplate
import template.PageTemplate
import java.time.LocalDateTime

abstract class HtmlTemplate(file: String, path: String,
                            val title: String = Strings.title,
                            val description: String = Strings.description,
                            val keywords: String = Strings.keywords,
                            val date: LocalDateTime? = null,
                            val header: BaseTemplate = Header,
                            val footer: BaseTemplate = Footer,
                            val author: String = Strings.author
                            ): PageTemplate(file, path) {

    abstract fun getArticle(): String

    val html by lazy {
        """
            <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="UTF-8">
                    <meta name="description" content="$description">
                    <meta name="keywords" content="$keywords">
                    <meta name="author" content="$author">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>$title</title>
                    <link rel="icon" href="favicon.ico">
                    <link rel="stylesheet" href="${Style.getFullPath()}">
                </head>
                <body>
                    $header
                    ${getArticle()}
                    $footer
                </body>
            </html>
        """
    }

    val hash by lazy { getHash(html) }

    override fun getCode()=
        """
            <!-- $hash -->
            $html
        """

    override fun isHashEqual(source: List<String>) =
        source.count() > 0 && source[0].trimStart(' ', '\t').trimEnd(' ', '\t') == "<!-- $hash -->"
}