/*
    StaticEngine - simple static site generator
    Copyright (C) 2018  Alex Arus (ArusAvS@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package template

import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.util.*

abstract class PageTemplate(val file: String, val path: String) : BaseTemplate() {

    fun getUrl() = Site.url + getFullPath()

    fun getFullPath() = if (path.isNotBlank()){ "/$path/$file" }else{ "/$file" }

    fun getHash(string: String): String {
        val messageDigest = MessageDigest.getInstance("SHA-256")
        messageDigest.update(string.toByteArray(StandardCharsets.UTF_8))
        return Base64.getEncoder().encodeToString(messageDigest.digest())
    }

    abstract fun isHashEqual(source: List<String>): Boolean
}